﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Sorciere : Monstre
    {
        public Sorciere(string nomsorc) : base(nomsorc) 
        {
            PDV = 30;
            PA = 55;
            Console.WriteLine("New sorciere");
        }

        public void attaquer(Personnage nomperso)
        {
            base.attaquer(nomperso);
        }

        public void subir(short dgtsubis)
        {
            base.subir(dgtsubis);
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
