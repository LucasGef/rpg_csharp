﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Partie
    {
        List<Monstre> monstres;
        Hero hero;
        short nbrmonstretue;

        public Partie(Hero lehero, List<Monstre> lesmonstres)
        {
            hero = lehero;
            monstres = lesmonstres;
            nbrmonstretue = 0;
        }

        private bool Baston(Personnage lemonstre) 
        {
            short quicommence = (short)new Random().Next(0, 1);
            bool quiletour = true;
            if (quicommence == 0)
                quiletour = false;
            bool resultatBaston;

            while (hero.PDV > 0 && lemonstre.PDV > 0)
            {

                if (quiletour)
                {
                    Console.WriteLine("\n Tour du héros \n ");
                    lemonstre.subir(hero.PA);
                }
                else
                {
                    Console.WriteLine("\n Tour du monstre \n");
                    hero.subir(lemonstre.PA);
                }

                quiletour = !quiletour;

                Console.WriteLine(hero.Nom + ", tes points de vie sont de : " + hero.PDV + "\nles point de vie de " + lemonstre.Nom + " sont de : " + lemonstre.PDV);
            }
            if (hero.PDV <= 0)
                resultatBaston = false;
            else
                resultatBaston = true;

            return resultatBaston;
        }

        private void coffredefin()
        {
            short hasard = (short)new Random().Next(5, 20);

            if (new Random().Next(0, 1) == 0)
                hero.Soin(hasard);
            else
                hero.subir(hasard);
        }

        private List<Monstre> GestionMonstres(List<Monstre> monstres)
        {
            for(int i=0; i < 10; i++)
            {
                Random alea = new Random();
                int num = alea.Next(1, 3);

                switch (num)
                {
                    case 1:
                        Sorciere sorc = new Sorciere("Sorciere");
                        monstres.Add(sorc);
                        break;
                    case 2:
                        Gobelins gob = new Gobelins("Gobelins");
                        monstres.Add(gob);
                        break;
                    case 3:
                        Skellette ske = new Skellette("Skelette");
                        monstres.Add(ske);
                        break;
                }
                Console.WriteLine(num);
            }

            return monstres;
        }

        public void Lapartie()
        {
            Console.WriteLine("\nDebut de partie\n");
            GestionMonstres(monstres);
            foreach(Personnage monstre in monstres)
            {
                Console.WriteLine("Voici " + monstre.Nom);
                if (hero.Etat)
                {
                    if (Baston(monstre))
                    {
                        Console.WriteLine("Bien jouer pour ta victoire");
                        nbrmonstretue++;
                        coffredefin();
                    }
                    else
                        Console.WriteLine("Nul, le monstre gagne");
                }
                else
                    Console.WriteLine("Tu est decede..");
            }

            Console.WriteLine("ton hero {0}, a tue {1} monstres.", hero.Nom, nbrmonstretue);
        }

    }
}
