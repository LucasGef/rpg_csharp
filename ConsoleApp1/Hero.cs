﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Hero : Personnage
    {
        public Hero(string nomhero, short pdvhero, short pahero) : base(nomhero, pdvhero, pahero)
        {
        }

        public void Soin(short pdv)
        {
            PDV += pdv;
        }

        /*public void becomeChevalier()
        {
            float PDVFinaux = this.PDV;
            PDVFinaux *= 1.20F;
            //this.PDV = int.Parse(PDVFinaux);
        }*/

        public void subir(short dgtsubis)
        {
            base.subir(dgtsubis);
        }

        public void attaquer(Personnage nommonstre)
        {
            base.attaquer(nommonstre);
        }
    }
}
