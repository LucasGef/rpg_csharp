﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public abstract class Personnage
    {
        string nom;
        short pdv, pa;
        bool etat;
        

        public Personnage(string nomperso)
        {
            nom = nomperso;
            etat = true;
        }

        public Personnage(string nomperso, short pdvperso, short paperso)
        {
            nom = nomperso;
            pdv = pdvperso;
            pa = paperso;
            etat = true;
        }

        public void subir(short dgtsubis)
        {
            PDV -= dgtsubis;
        }

        public void attaquer(Personnage nomperso)
        {
            nomperso.PDV -= PA;
        }

        public short PDV
        { 
            get => pdv;
            
            set
            {
                pdv = value;
                if (PDV <= 0) Etat = false;
            }
        }

        public override string ToString()
        {
            return $"Je suis {Nom}, j'ai {PDV} point de vie et {PA} point d'attaque";
        }

        public short PA { get => pa; set => pa = value; }
        public string Nom { get => nom; set => nom = value; }
        public bool Etat { get => etat; set => etat = value; }
    }
}
