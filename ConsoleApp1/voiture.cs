﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Voiture
    {
        private string Constructeur_, VitesseM_;
        private byte Vitesse_, Allure_, Chevaux_;
        private Human Conducteur_;

        public Voiture(string constructeur, string vitesseM, byte chevaux, Human conducteur)
        {
            Constructeur = constructeur;
            VitesseM = vitesseM;
            Vitesse = 0;
            Allure = 0;
            Chevaux = chevaux;
            Conducteur = conducteur;
        }

        public string PresenterLesPapiers()
        {
            return "Mon constructeur est : " + Constructeur + ", mon vehicule a " + Chevaux.ToString() + " chevaux, " + Conducteur.ToString();
        }

        public string vitesseactuelle()
        {
            return "la vitesse de ma voiture est actuellement de " + Allure + " et ma vitesse est la " + Vitesse;
        }
        private void ChangerVit()
        {
            switch (Allure)
            {
                case byte vit when vit < 20:
                    Vitesse = 1;
                    break;
                case byte vit when vit > 20 && vit < 40:
                    Vitesse = 2;
                    break;
                case byte vit when vit > 40 && vit < 60:
                    Vitesse = 3;
                    break;
                case byte vit when vit > 60 && vit < 80:
                    Vitesse = 4;
                    break;
                case byte vit when vit > 80 && vit < 100:
                    Vitesse = 5;
                    break;
                case byte t_speed when t_speed > 100:
                    Vitesse = 6;
                    break;
                default:
                    Vitesse = 0;
                    break;
            }
        }

        public void Accelerer(byte Augmentation)
        {
            Allure += Augmentation;
            ChangerVit();
        }

        public void Freiner(byte Ralentissement)
        {
            Allure -= Ralentissement;
            ChangerVit();
        }


        public string Constructeur 
        { 
            get => Constructeur_;
            set => Constructeur_ = value; 
        }

        public string VitesseM
        { 
            get => VitesseM_;
            set => VitesseM_ = value;
        }

        public byte Chevaux
        { 
            get => Chevaux_;
            set => Chevaux_ = value;
        }

        public byte Allure 
        { 
            get => Allure_; 
            set => Allure_ = value; 
        }

        public byte Vitesse 
        { 
            get => Vitesse_; 
            set => Vitesse_ = value  <= 6 ? value : throw new ArgumentException("Il n'y a que 6 vitesse, tu est au dessus"); 
        }

        public Human Conducteur 
        { 
            get => Conducteur_; 

            set => Conducteur_ = value; 
        }
    }
}
