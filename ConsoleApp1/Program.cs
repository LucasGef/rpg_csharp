﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Hero hero1 = new Hero("Lucas", 100, 50);

            List<Monstre> monstres = new List<Monstre>();

            Partie partie1 = new Partie(hero1, monstres);

            partie1.Lapartie();

            Console.ReadKey();
        }
    }
}
