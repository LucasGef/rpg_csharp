﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace ClassLibrary1
{
    class Human
    {
        String nom, prenom;
        byte age;
        public Human(String nom,String prenom,byte age)
        {
            this.nom = nom;
            this.prenom = prenom;
            this.age = age;
        }

        public override string ToString()
        {
            return"je m'appelle " + nom + " " + prenom + " et j'ai " + age + " ans";
        }
    }
}
